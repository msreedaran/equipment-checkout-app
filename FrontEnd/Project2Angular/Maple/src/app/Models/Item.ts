export class Item {
   i_id: number;
   i_name: string;
   i_avail: number;
   i_pic: string;

   constructor(i_id: number, i_name: string, i_avail: number, i_pic: string) {
      this.i_id = i_id;
      this.i_name = i_name;
      this.i_avail = i_avail;
      this.i_pic = i_pic;
   }
}
export class Transaction {
    t_id:number;
    t_item_id:number;
    t_timeout:string;
    t_timein:string;
    t_borrower:number;
    t_manokout:number;
    t_manokin:number;

    constructor(t_id:number, t_item_id:number, t_timeout:string, t_timein:string, t_borrower:number, t_manokout:number, t_manokin:number) {
        this.t_id = t_id;
        this.t_item_id = t_item_id;
        this.t_timeout = t_timeout;
        this.t_timein = t_timein;
        this.t_borrower = t_borrower;
        this.t_manokout = t_manokout;
        this.t_manokin = t_manokin;
    }
}


export class Employee {
   e_id: number;
   e_username: string;
   e_password: string;
   e_fullname: string;
   e_isManager: number;
   e_contact: string;

   constructor(e_id: number, e_username: string, e_password: string, e_fullname: string, e_isManager: number, e_contact: string) {
      this.e_id = e_id;
      this.e_username = e_username;
      this.e_password = e_password;
      this.e_fullname = e_fullname;
      this.e_isManager = e_isManager;
      this.e_contact = e_contact;
   }
}


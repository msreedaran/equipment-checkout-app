import { Injectable } from '@angular/core';
import { Item } from '../Models/Item';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
   providedIn: 'root'
})
export class ItemService {

   constructor(private http: HttpClient) { }

   // to finish
   addItem(item: Item): Observable<boolean> {

      const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
      let body = JSON.stringify(item);
      let GetLink = "http://localhost:8080/Project_2/Items/AddItem";

      return this.http.post<boolean>(GetLink, body, { headers })
   }

   // to finish
   updateItem(item: Item): Observable<boolean> {

      const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
      let body = JSON.stringify(item);

      return this.http.post<boolean>("put url here", body, { headers })
   }

   // to finish
   getItem(id: number): Observable<Item> {
      const params: string = `/${id}`
      // return Observable<Item>
      return this.http.get<Item>('url' + params);
   }

   getAllItems(): Observable<Item[]> {
      return this.http.get<Item[]>("http://localhost:8080/Project_2/Items/AllItems");
   }

   getAvailableItems(): Observable<Item[]> {
      return this.http.get<Item[]>("http://localhost:8080/Project_2/Items/AvailableItems");
   }

   deleteItem(id: number): Observable<boolean> {
      return this.http.delete<boolean>("http://localhost:8080/Project_2/Items/DeleteItem/" + id);
   }

}

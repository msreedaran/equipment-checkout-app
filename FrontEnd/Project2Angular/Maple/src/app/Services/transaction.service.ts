import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Transaction } from '../Models/Transaction';

@Injectable({
   providedIn: 'root'
})
export class TransactionService {


   constructor(private http: HttpClient) { }

   // to finish
   addTransaction(transaction: Transaction): Observable<boolean> {

      const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
      let body = JSON.stringify(transaction);

      return this.http.post<boolean>("put url here", body, { headers })
   }

   // to finish
   updateTransaction(transaction: Transaction): Observable<boolean> {

      const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
      let body = JSON.stringify(transaction);

      return this.http.post<boolean>("put url here", body, { headers })
   }

   // to finish
   getInTransaction(t_manokout: number): Observable<Transaction[]> {
      const params: string = `${t_manokout}`
      // return Observable<Transaction>
      return this.http.get<Transaction[]>("http://localhost:8080/Project_2/Transactions/InTransactions/" + params);
   }

   getOutTransaction(t_manokin: number): Observable<Transaction[]> {
      console.log(t_manokin);
      const params: string = `/${t_manokin}`
      // return Observable<Transaction>
      return this.http.get<Transaction[]>("http://localhost:8080/Project_2/Transactions/InTransactions" + params);
   }

   getAllTransactions(): Observable<Transaction[]> {
      return this.http.get<Transaction[]>("http://localhost:8080/Project_2/Transactions/AllTransactions");
   }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Employee } from '../Models/Employee';

@Injectable({
   providedIn: 'root'
})
export class EmployeeService {

   constructor(private http: HttpClient) { }
   private getUserNameFile = "http://localhost:8080/Project_2/Employees";
   private headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

   login(Username: string, Password: string): Observable<Employee> {

      const body: string = `username=${Username}&password=${Password}`;
         //console.log(body);
         //console.log(this.getUserNameFile + body);
      return this.http.post<Employee>(this.getUserNameFile, body, { headers: this.headers, withCredentials: false }).pipe(map(resp => resp as Employee));
      
   }
   

   getEmployee(Username: string): Observable<Employee> {
      const param: string = `?username=${Username}`;
      
      return this.http.get<Employee>(this.getUserNameFile + param);


   }
}


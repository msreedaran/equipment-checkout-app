import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from 'src/app/Services/item.service';
import { TransactionService } from 'src/app/Services/transaction.service';
import { Item } from 'src/app/Models/Item';
import { Transaction } from 'src/app/Models/Transaction';

@Component({
   selector: 'app-manager-page',
   templateUrl: './manager-page.component.html',
   styleUrls: ['./manager-page.component.css']
})
export class ManagerPageComponent implements OnInit {

   State = State; // lets us use enum at bottom of this file
   page: State = State.Choices; // what 'page' we are currently on
   unsortedItemList: Item[] = null;
   unsortedRequestList: Transaction[] = null;
   sortedItemList: Item[] = null;
   sortedRequestList: Transaction[] = null;
   id: number = null;

   pic: string = null;
   name: string = null;


   constructor(
      private router: Router,
      private itemService: ItemService,
      private transactionService: TransactionService) { }

   ngOnInit() { }




   /* VIEWING METHODS */
   viewItems() {
      this.itemService.getAllItems().subscribe(
         (response) => {
            this.unsortedItemList = response;
            console.log(this.unsortedItemList);
            if (null === response || undefined === response) {
               alert("Failed to retrieve any items.")
            } else {
               this.sortedItemList = this.unsortedItemList.sort(
                  (a, b) => { return a.i_id - b.i_id });
               this.page = State.ViewItems;
            }
         },
         (response) => {
            console.log("Error sending submitItem() request");
            alert("Failed to retrieve any items.")
         }
      )
   }

   viewRequests() {
      this.transactionService.getAllTransactions().subscribe(
         (response) => {
            this.unsortedRequestList = response;
            if (null === response || undefined === response) {
               alert("Failed to retrieve any requests.")
            } else {
               this.sortedRequestList = this.unsortedRequestList.sort(
                  (a, b) => { return a.t_id - b.t_id });
               this.page = State.ViewRequests;
            }
         },
         (response) => {
            console.log("Error sending viewRequests() request");
            alert("Failed to retrieve any requests.")
         }
      )
   }

   viewHistory() {
      this.transactionService.getAllTransactions().subscribe(
         (response) => {
            console.log(response);
            this.unsortedRequestList = response;
            if (null === response || undefined === response) {
               alert("Failed to retrieve any history.")
            } else {
               this.sortedRequestList = this.unsortedRequestList.sort(
                  (a, b) => { return a.t_id - b.t_id });
               this.page = State.ViewHistory;
            }
         },
         (response) => {
            console.log("Error sending viewHistory() request");
            alert("Failed to retrieve any history.")
         }
      )
   }




   /* PROCESSING METHODS */

   submitAddItem() {
      let i: Item = new Item(-1, this.name, 1, this.pic);
      console.log('submitting: ');
      console.log(i);

      if (null === i.i_pic || undefined === i.i_pic) {
         i.i_pic = "noURL";
      }
      this.itemService.addItem(i).subscribe(
         (response) => {
            console.log(response);
            if (true == response) {
               alert("Item Succesfully Submitted");
            } else {
               alert("Error submitting item.");
            }
         },
         (response) => {
            console.log("Error sending submitAddItem() request");
            alert("Error submitting item.");
         }
      )
      this.name = null;
      this.pic = null;
   }

   submitDeleteItem() {

      this.itemService.deleteItem(this.id).subscribe(
         (response) => {
            console.log(response);
            if (true == response) {
               alert("Requested item has been deleted.");
            } else {
               alert("Requested item failed to delete.");
            }
         },
         (response) => {
            console.log("Error sending submitDeleteItem() request");
            alert("Requested item failed to delete.");
         }
      )


      this.id = null;

   }

   submitProcessRequest() {
      // send request processing to backend
   }




   /* PAGE NAVIGATION LOGIC */

   addItem() {
      this.page = State.ItemAdd;
   }

   deleteItem() {
      this.page = State.ItemDelete;
   }
   processRequest() {
      this.page = State.ProcessRequest;
   }

   goBack() {
      // if at first page, just log out
      if (this.page === State.Choices) {
         this.logOut();
      }

      // if processing, then go back to viewing requests list
      else if (this.page === State.ProcessRequest) {
         this.page = State.ViewRequests;
      }

      // else go back to first page
      else {
         this.page = State.Choices;
         this.resetPageVariables();
      }
   }

   logOut() {
      localStorage.removeItem("session_user"); // clear user session information

      this.page = State.Choices;
      this.resetPageVariables();

      this.router.navigateByUrl('/login'); // return to login screen
   }




   /* MISC. HELPER FUNCTIONS */

   resetPageVariables() {
      this.unsortedItemList = null;
      this.unsortedRequestList = null;
      this.sortedItemList = null;
      this.sortedRequestList = null;
      this.id = null;

      this.pic = null;
      this.name = null;

   }
}
export enum State {
   Choices, ViewItems, ItemAdd,
   ItemDelete, ViewRequests, ViewHistory,
   ProcessRequest
};
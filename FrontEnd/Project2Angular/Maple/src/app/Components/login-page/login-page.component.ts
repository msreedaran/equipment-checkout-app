import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/Services/employee.service';
import { Router } from '@angular/router';
// import { EmployeeModel } from 'src/app/Models/Employee';

@Component({
   selector: 'app-login-page',
   templateUrl: './login-page.component.html',
   styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent implements OnInit {

   username: string;
   password: string;

   constructor(private logins: EmployeeService, private router: Router) { }

   ngOnInit() { }

   LogIn() {
      this.logins.login(this.username, this.password).subscribe(
         (response) => {
            console.log("Login request delivered.");
            console.log(response);
            if (response.e_id !== 0) {
               localStorage.setItem("session_user", JSON.stringify(response));
               if (1 === response.e_isManager)
                  this.router.navigateByUrl('/manager');
               else
                  this.router.navigateByUrl('/associate');
            } else {
               console.log("Invalid login credentials.");
            }
         },

         (response) => {
            console.log("Invalid login credentials.");
         }
      );
   }
}


   // LogIn() {
   //    this.logins.login(this.Username, this.Password).subscribe(
   //       (response) => {
   //          this.User = response;
   //          Object.assign(this.User, { response });
   //          console.log(response);
   //          console.log(this.User.e_isManager);




   //          if (this.User.e_id == 0) {
   //             this.loggedIn = true;



   //             this.Invalid = "You have entered an invalid Username or Password";
   //          }
   //          else {
   //             this.loggedIn = false;
   //             if (this.User.e_isManager == 1) {
   //                this.isUser = 1;
   //                this.router.navigate(['manager']);

   //             }
   //             else {
   //                this.isUser = 0
   //                this.router.navigate(['user']);
   //             }
   //          }
   //       }), (error) => { }
   //    }

// }

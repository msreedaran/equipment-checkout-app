import { Component, OnInit } from '@angular/core';
// import { longStackSupport } from 'q';
import { Router } from '@angular/router';
import { ItemService } from 'src/app/Services/item.service';
import { TransactionService } from 'src/app/Services/transaction.service';
import { Item } from 'src/app/Models/Item';
import { Transaction } from 'src/app/Models/Transaction';
import { Employee } from 'src/app/Models/Employee';

@Component({
   selector: 'app-associate-page',
   templateUrl: './associate-page.component.html',
   styleUrls: ['./associate-page.component.css']
})

// the associate page allows associates to checkin and
// checkout stuff. the page HTML will change to simulate
// different "pages", e.g. upon clicking "check in" the
// associate will be taken to a form to submit a checkin
// request. the HTML change is determined by a Switch
// which checks the variable 'page' to know which HTML
// the associate should be seeing currently

// there should be a back button on each 'page' (each switch case HTML block)
// that calls goBack(), except on the first page where it will say
// "Log Out" and call either goBack() or logOut()
export class AssociatePageComponent implements OnInit {

   State = State; // lets us use enum at bottom of this file
   page: State = State.Choices; // what 'page' we are currently on
   transactionList: Transaction[];
   itemList:Item[];
   hideTable: boolean = true;
   manokout:number;
   manokin:number;
   user:Employee;
   unsortedItemList: Item[] = null;
   sortedItemList: Item[] = null;
   id: number = null;

   constructor(private router: Router,
      private itemService: ItemService,
      private transactionService: TransactionService) { }

   ngOnInit() {
   }




   // insert methods here

   checkIn() {
      this.user = JSON.parse(localStorage.getItem("session_user")); 
      this.manokin = this.user.e_id;
      console.log(this.manokin);
      this.transactionService.getInTransaction(this.manokin).subscribe(
         (response) => {
            this.hideTable = false;
            this.transactionList = response;
            console.log(this.transactionList);
            
         },
         (response) => { console.log("Something went wrong"); }
      )

      this.id = null;

      this.page = State.CheckIn;
   }

   submitCheckIn() {

      this.manokin = -1;

      this.id = null;

   }

   checkOut() {

      this.itemService.getAllItems().subscribe(
         (response) => {
            this.unsortedItemList = response;
            console.log(this.unsortedItemList);
            if (null === response || undefined === response) {
               alert("Failed to retrieve any items.")
            } else {
               this.sortedItemList = this.unsortedItemList.sort(
                  (a, b) => { return a.i_id - b.i_id });
               this.page = State.CheckOut;
            }
         },
         (response) => {
            console.log("Error sending submitItem() request");
            alert("Failed to retrieve any items.")
         }
      )
      
      // this.user = JSON.parse(localStorage.getItem("session_user")); 
      // this.manokout = this.user.e_id;
      // console.log(this.manokout);
      // this.itemService.getAvailableItems().subscribe(
      //    (response) => {
      //       this.itemList = response;
      //       console.log(response);
      //    },
      //    (response) => { console.log("Something went wrong"); }
      // )

      this.id = null;

      this.page = State.CheckOut;
   }

   submitCheckOut() {

      this.id = null;

   }

   seeAvailable() {




      this.page = State.SeeAvailable;
   }


   goBack() {

      this.hideTable = true;

      // if at first page, just log out
      if (this.page === State.Choices) {
         this.logOut();
      }
      // else go back to first page
      else {
         this.page = State.Choices;
      }
   }

   logOut() {
      localStorage.removeItem("session_user"); // clear user session information

      this.page = State.Choices; // reset page variable
      this.router.navigateByUrl('/login'); // return to login screen
   }

}
export enum State { Choices, CheckIn, CheckOut, SeeAvailable };
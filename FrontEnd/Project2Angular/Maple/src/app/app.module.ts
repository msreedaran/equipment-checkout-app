import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageComponent } from './Components/login-page/login-page.component';
import { AssociatePageComponent } from './Components/associate-page/associate-page.component';
import { ManagerPageComponent } from './Components/manager-page/manager-page.component';

@NgModule({
   imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule,
      HttpClientModule
   ],
   declarations: [
      AppComponent,
      AssociatePageComponent,
      LoginPageComponent,
      AssociatePageComponent,
      ManagerPageComponent
   ],
   providers: [],
   bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssociatePageComponent } from './Components/associate-page/associate-page.component';
import { ManagerPageComponent } from './Components/manager-page/manager-page.component';
import { LoginPageComponent } from './Components/login-page/login-page.component';


const routes: Routes = [
   { path: '', redirectTo: '/login', pathMatch: 'full' },
   { path: "associate", component: AssociatePageComponent },
   { path: "manager", component: ManagerPageComponent },
   { path: 'login', component: LoginPageComponent }
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
})
export class AppRoutingModule { }

# Revature Training 1904JFS Project 2 - Team Maple - Equipment Checkout App

## Members
* Database, Architecture - Berson, Zack
* UI - Dixon, Terrence
* Spring, Services - Fittipaldi, Nick
* Spring, Servlets - Orr, John
* Scrummaster, DevOps - Sreedaran, Manjunath

We are Team Maple. This is Project 2 of Revature training batch 1904JFS. Follow our progress live! https://trello.com/b/FUb8XoDh/equipment-checkout-app

We are building a full-stack application that allows associates to request equipment for checkout and managers to approve/deny requests. All team members are involved at all layers of the development stack with some members focused on a particular layer or role.

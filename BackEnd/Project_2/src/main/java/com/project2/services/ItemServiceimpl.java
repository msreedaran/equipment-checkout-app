
package com.project2.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.project2.beans.Item;
import com.project2.daos.ItemDAO;



public class ItemServiceimpl implements ItemService {

    @Autowired
    ItemDAO call;

    public List<Item> getAllItemsService() {
        List<Item> result = call.getAllItems();
        return result;
    }

    public List<Item> getAvailableItemsService() {
        List<Item> result = call.getAvailableItems();
        return result;
    }

    public boolean addItemService(Item item) {
        boolean tf = call.addItem(item);
        return tf;
    }

    public boolean deleteItemService(int itemId) {
        boolean tf = call.deleteItem(itemId);
        return tf;
    }

	public boolean updateItemService(int itemId, int avail) {
		boolean tf = call.updateItem(itemId, avail);
		return tf;
	}
}

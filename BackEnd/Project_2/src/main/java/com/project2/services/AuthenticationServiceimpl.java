package com.project2.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.project2.beans.Employee;
import com.project2.daos.UserDAO;

public class AuthenticationServiceimpl implements AuthenticationService {

    @Autowired
	UserDAO call;
    
    
	public Employee checkCredentials(String username, String password) {
		Employee john;
		
		john = call.getUser(username, password);
		
		return john;
	}

}

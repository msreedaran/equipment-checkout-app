package com.project2.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.project2.beans.Employee;
import com.project2.beans.Transaction;
import com.project2.daos.TransactionDAO;


public class TransactionServiceImpl implements TransactionService {
	
    @Autowired
	TransactionDAO td;

	public List<Transaction> getAllTransactions() {

		List<Transaction> result = td.getAllTransactions();
		return result;
	}

	public List<Transaction> getInTransactions(int managerInId) {
		
	    Employee e = new Employee();
	    e.setE_id(managerInId);
		List<Transaction> result = td.getInTransactions(managerInId);
		return result;
	}

	public List<Transaction> getOutTransactions(int managerOutId) {
		
        Employee e = new Employee();
        e.setE_id(managerOutId);
		List<Transaction> result = td.getOutTransactions(managerOutId);
		return result;
	}

	public boolean addTransactions(Transaction transaction) {

		boolean b = td.addTransactions(transaction);
		return b;
	}

	public boolean delTransactions(int id) {
		
	    Transaction t = new Transaction();
	    t.setId(id);
		boolean dt = td.delTransactions(id);
		return dt;
	}

	public boolean checkOut(int t_manokout, int t_id) {
		
		Transaction t = new Transaction();
	    t.setManagerOutId(t_manokout);
	    t.setId(t_id);
		boolean dt = td.checkOut(t_manokout, t_id);
		return dt;
	}

	public boolean checkIn(int t_manokin, int t_id) {
		
		Transaction t = new Transaction();
	    t.setManagerInId(t_manokin);
	    t.setId(t_id);
		boolean dt = td.checkOut(t_manokin, t_id);
		return dt;
	}


}

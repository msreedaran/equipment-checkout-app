package com.project2.services;

import java.util.List;

import com.project2.beans.Item;

public interface ItemService {

	public List<Item> getAllItemsService();
	public List<Item> getAvailableItemsService();
	public boolean addItemService(Item item);
	public boolean deleteItemService(int itemId);
	public boolean updateItemService(int itemId, int avail);
}


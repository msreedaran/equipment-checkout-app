package com.project2.services;

import com.project2.beans.Employee;

public interface AuthenticationService {
    public Employee checkCredentials(String username, String password);
}

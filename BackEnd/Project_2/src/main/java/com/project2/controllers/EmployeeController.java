package com.project2.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project2.beans.Employee;
import com.project2.services.AuthenticationService;

@CrossOrigin(origins = "*", allowCredentials="true")
@RestController
public class EmployeeController {

    

    @Autowired
    AuthenticationService j;
    @RequestMapping(value = "/Employees", method = RequestMethod.POST)
    public Employee allAssociates(HttpSession session, String username,String password){

        Employee e = j.checkCredentials(username, password);

        session.setAttribute("user", e);
        return e;

    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() { return "hello"; }
    

}


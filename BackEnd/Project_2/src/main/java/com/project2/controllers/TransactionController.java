package com.project2.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project2.beans.Transaction;
import com.project2.services.TransactionService;

@CrossOrigin(origins = "*", allowCredentials="true")
@RestController
public class TransactionController {

	@Autowired
	TransactionService ts;
	
	@RequestMapping(value = "/Transactions/AllTransactions", method = RequestMethod.GET)
	public List<Transaction> getAllTransactions() {
		return ts.getAllTransactions();
	}
	
	@RequestMapping(value = "/Transactions/DeleteTransaction/{id}", method = RequestMethod.DELETE)
	public boolean delTransaction(@PathVariable int id){
		return ts.delTransactions(id);
	}


	@RequestMapping(value = "/Transactions/AddTransaction", method = RequestMethod.POST)
	public boolean addTransaction(@RequestBody Transaction transaction) {
		return ts.addTransactions(transaction);
	}
	
	@RequestMapping(value = "/Transactions/InTransactions/{managerInId}", method = RequestMethod.GET)
	public List<Transaction> getInTransactions(@PathVariable int managerInId) {
		
		return ts.getInTransactions(managerInId);
	}
	
	@RequestMapping(value = "/Transactions/OutTransactions/{managerOutId}", method = RequestMethod.GET)
	public List<Transaction> getOutTransactions(@PathVariable int managerOutId) {
		return ts.getOutTransactions(managerOutId);
	}
	
	@RequestMapping(value = "/Transactions/CheckInTransactions/{t_manokin}/{t_id}", method = RequestMethod.POST)
	public boolean checkIn(@PathVariable int t_manokin, int t_id) {
		return ts.checkIn(t_manokin, t_id);
	}
	
	@RequestMapping(value = "/Transactions/CheckOutTransactions/{t_manokout}/{t_id}", method = RequestMethod.POST)
	public boolean checkOut(@PathVariable int t_manokout, int t_id) {
		return ts.checkOut(t_manokout, t_id);
	}
	
	
}

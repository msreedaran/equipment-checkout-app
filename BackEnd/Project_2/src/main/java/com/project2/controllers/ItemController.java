package com.project2.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project2.beans.Item;
import com.project2.services.ItemService;


@CrossOrigin(origins = "*", allowCredentials="true")
@RestController
public class ItemController {

	@Autowired
	ItemService is;
	
	@RequestMapping(value = "Items/AllItems", method = RequestMethod.GET)
	public List<Item> getAll() {
		return is.getAllItemsService();
	}
	
	@RequestMapping(value = "Items/AvailableItems", method = RequestMethod.GET)
	public List<Item> getAvailable() {
		return is.getAvailableItemsService();
	}


	@RequestMapping(value = "Items/AddItem", method = RequestMethod.POST)
	public boolean addItem(@RequestBody Item item) {
		return is.addItemService(item);
	}
	
	@RequestMapping(value = "Items/DeleteItem/{itemId}", method = RequestMethod.DELETE)
	public boolean deleteItem(@PathVariable int itemId) {
		return is.deleteItemService(itemId);
	}
	
	@RequestMapping(value = "Items/UpdateItem/{itemId}/{avail}", method = RequestMethod.POST)
	public boolean updateItem(@PathVariable int itemId, int avail) {
		return is.updateItemService(itemId, avail);
	}
}

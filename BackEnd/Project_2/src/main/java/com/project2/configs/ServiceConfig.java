package com.project2.configs;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.project2.daos.ItemDAO;
import com.project2.daos.ItemDAOimpl;
import com.project2.daos.TransactionDAO;
import com.project2.daos.TransactionDAOImpl;
import com.project2.daos.UserDAO;
import com.project2.daos.UserDAOimpl;
import com.project2.services.AuthenticationService;
import com.project2.services.AuthenticationServiceimpl;
import com.project2.services.ItemService;
import com.project2.services.ItemServiceimpl;
import com.project2.services.TransactionService;
import com.project2.services.TransactionServiceImpl;

@Configuration
public class ServiceConfig {
	
	@Bean
	public AuthenticationService getAuthentication() {
		return new AuthenticationServiceimpl();
	}
	@Bean
	public UserDAO getDAO() {
		return new UserDAOimpl();
	}
	@Bean
	public ItemDAO getItemDAO() {
		return new ItemDAOimpl();
		
	}
	@Bean
	public ItemService getservice() {
		return new ItemServiceimpl();
	}
	@Bean
	public TransactionService getTranService() {
		return new TransactionServiceImpl();
	}
	@Bean
	public TransactionDAO getTransDAO() {
		return new TransactionDAOImpl();
	}
	
	@Bean
	public SessionFactory getSessionFactory() {
	    return new org.hibernate.cfg.Configuration().configure().buildSessionFactory();
	}

}

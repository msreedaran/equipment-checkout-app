package com.project2.daos;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.project2.beans.Item;

public class ItemDAOimpl implements ItemDAO {
	@Autowired
	SessionFactory sf;// = HibernateUtil.getSessionFactory();
	
	public List<Item> getAllItems() {
		Session sess = sf.openSession();
	    String hql = "FROM Item a";

	    Query q = sess.createQuery(hql);
	    
	    @SuppressWarnings("unchecked")
        List<Item> result = (List<Item>) q.getResultList();
	    
	    return result;
	}
	
	public List<Item> getAvailableItems() {
		Session sess = sf.openSession();
	    String hql = "FROM Item a where a.I_avail = 1";
	        
	    Query q = sess.createQuery(hql);

	    @SuppressWarnings("unchecked")
        List<Item> result = (List<Item>) q.getResultList();
	    sess.close();
	    
	    return result;
	}
	
	public boolean addItem(Item item) {
		Session sess = sf.openSession();
	    sess.beginTransaction();

	    sess.save(item);
	        
	    sess.getTransaction().commit();
	    sess.close();
	    
	    return true;
	}

	public boolean deleteItem(int itemId) {
		Session sess = sf.openSession();
		sess.beginTransaction();
	    String hql = "DELETE FROM Item WHERE I_id = :id";

	    Query q = sess.createQuery(hql);
	    q.setParameter("id", itemId);
	    q.executeUpdate();

	    sess.getTransaction().commit();
	    sess.close();
	        
	    return true;
	}

	public boolean updateItem(int itemId, int avail) {
		Session sess = sf.openSession();
		sess.beginTransaction();
	    String hql = "UPDATE Item SET I_avail = :avail where I_id = :id";
	    
	    Query q = sess.createQuery(hql);
	    q.setParameter("avail", avail);
	    q.setParameter("id", itemId);
	    q.executeUpdate();
	    
	    sess.getTransaction().commit();
	    sess.close();
	    
		return true;
	}
}




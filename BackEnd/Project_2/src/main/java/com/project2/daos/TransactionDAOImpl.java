package com.project2.daos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.project2.beans.Transaction;



public class TransactionDAOImpl implements TransactionDAO {
	@Autowired
	SessionFactory sf; //= HibernateUtil.getSessionFactory();

	
	// Get all transactions 
	
	public List<Transaction> getAllTransactions() {
		
	    
		Session sess = sf.openSession();
		String hql = "FROM Transaction a";
		Query q = sess.createQuery(hql);
		List<Transaction> result = new ArrayList<Transaction>();
		
		// proper type checking from List to List<Transaction>
		for(Object t : q.getResultList()) {
		    result.add((Transaction)t);
		}
		
		return result;
	}

	// Return all transactions of items that have been checked in
	
	public List<Transaction> getInTransactions(int managerInId) {
		
		Session sess = sf.openSession();
		String hql = "FROM Transaction a where a.t_borrower = :borrower and a.t_manokout = 1";
		Query q = sess.createQuery(hql);
		q.setParameter("borrower", managerInId);
		
		List<Transaction> result = new ArrayList<Transaction>();
		
        // proper type checking from List to List<Transaction>
        for(Object t : q.getResultList()) {
            result.add((Transaction)t);
        }
		return result;
	}
	
	// Return all transactions of items that have been checked out

	public List<Transaction> getOutTransactions(int managerOutId) {

		Session sess = sf.openSession();
		String hql = "FROM Transaction a where a.t_borrower = :borrower and a.t_manokout = ";
		Query q = sess.createQuery(hql);
		q.setParameter("managerOutId", managerOutId);
		
		ArrayList<Transaction> result = new ArrayList<Transaction>();
        
        // proper type checking from List to List<Transaction>
        for(Object t : q.getResultList()) {
            result.add((Transaction)t);
        }
        return result;
	}
	
	// Add a transaction to the database

	public boolean addTransactions(Transaction transaction) {
		
		Session sess = sf.openSession();
        sess.beginTransaction();
        sess.save(transaction);
        sess.getTransaction().commit();
        sess.close();
        return true;
	}
	
	// Remove a transaction from the database

	public boolean delTransactions(int id) {

		
		Session sess = sf.openSession();
		sess.beginTransaction();
        String hql = "DELETE FROM Transaction WHERE t_id = :id";
        Query q = sess.createQuery(hql);
        q.setParameter("id", id);
        q.executeUpdate();
        sess.getTransaction().commit();
        sess.close();
        return true;
	}

	public boolean checkOut(int t_manokout, int t_id) {
		
		Session sess = sf.openSession();
		sess.beginTransaction();
        String hql = "UPDATE Transaction SET t_manokout = :manokin where t_id = :t_id";
        Query q = sess.createQuery(hql);
        q.setParameter("t_manokout", t_manokout);
        q.setParameter("t_id", t_id);
        q.executeUpdate();
        sess.getTransaction().commit();
        sess.close();
        return true;
	}

	public boolean checkIn(int t_manokin, int t_id) {
		
		Session sess = sf.openSession();
		sess.beginTransaction();
        String hql = "UPDATE Transaction SET t_manokin = :manokin where t_id = :t_id";
        Query q = sess.createQuery(hql);
        q.setParameter("t_manokin", t_manokin);
        q.setParameter("t_id", t_id);
        q.executeUpdate();
        sess.getTransaction().commit();
        sess.close();
        return true;
	}


}

package com.project2.daos;

import java.util.List;

import com.project2.beans.Item;

public interface ItemDAO {

	public List<Item> getAllItems();
	public List<Item> getAvailableItems();
	public boolean addItem(Item item);
	public boolean deleteItem(int itemId);
	public boolean updateItem(int itemId, int avail);
}

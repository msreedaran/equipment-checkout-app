package com.project2.daos;

import java.util.List;

import com.project2.beans.Transaction;

public interface TransactionDAO {
	
	public List<Transaction> getAllTransactions();
	public List<Transaction> getInTransactions(int managerInId);
	public List<Transaction> getOutTransactions(int managerOutId);
	public boolean addTransactions(Transaction transaction);
	public boolean delTransactions(int id);
	public boolean checkOut(int t_manokout, int t_id);
	public boolean checkIn (int t_manokin, int t_id);
	

}
package com.project2.daos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.project2.beans.Employee;


public class UserDAOimpl implements UserDAO{

    @Autowired
	SessionFactory sf;
	
	public Employee getUser(String Username, String Password) {
		Session sess = sf.openSession();
		String hql = "FROM Employee a where a.e_username = :Username and a.e_password = :Password";
		Query q = sess.createQuery(hql);
		q.setParameter("Username", Username); //sets the :Username parameter for the hql query.
		q.setParameter("Password", Password); //Sets the :Password parameter for the hql query.
		List<Employee> resultList = new ArrayList<Employee>();
		
		// proper type casting for untyped List to List<Employee>
		for(Object e : q.getResultList()) {
		    resultList.add((Employee)e);
		}
		
		if(resultList.isEmpty()) return new Employee(0, "none", "none", "none", 0, "none"); //Dummy User sent.
		else return resultList.get(0);	
	}
			
}

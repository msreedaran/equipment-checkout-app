package com.project2.daos;

import com.project2.beans.Employee;

public interface UserDAO {

	public Employee getUser(String Username, String Password);
}

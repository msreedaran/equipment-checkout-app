package com.project2.beans;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Transactions")
public class Transaction {

   @Id
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_id")
   @SequenceGenerator(name = "t_id", sequenceName = "gen_tran_id", allocationSize = 1)
   private int t_id = -1;

   @Column(nullable = false)
   private int t_item_id;

   @Column(nullable = false)
   private Date t_timeout;

   private Date t_timein;
   {
       Calendar c = Calendar.getInstance();
       c.set(1, 1, 1);
       t_timeout = c.getTime();
       t_timein = c.getTime();
   }

   @Column(nullable = false)
   private int t_borrower;

   @Column(nullable = false)
   private int t_manokout;

   private int t_manokin = 0;

   public Transaction() {
      super();
   }

   public int getId() {
      return t_id;
   }

   public void setId(int id) {
      this.t_id = id;
   }

   public int getItemId() {
      return t_item_id;
   }

   public void setItemId(int itemId) {
      this.t_item_id = itemId;
   }

   public Date getTimeOut() {
      return t_timeout;
   }

   public void setTimeOut(Date timeOut) {
      this.t_timeout = timeOut;
   }

   public Date getTimeIn() {
      return t_timein;
   }

   public void setTimeIn(Date timeIn) {
      this.t_timein = timeIn;
   }

   public int getEmployeeId() {
      return t_borrower;
   }

   public void setEmployeeId(int employeeId) {
      this.t_borrower = employeeId;
   }

   public int getManagerOutId() {
      return t_manokout;
   }

   public void setManagerOutId(int managerOutId) {
      this.t_manokout = managerOutId;
   }

   public int getManagerInId() {
      return t_manokin;
   }

   public void setManagerInId(int managerInId) {
      this.t_manokin = managerInId;
   }

   @Override
   public String toString() {
      return "TransactionBean [id=" + t_id + ", itemId=" + t_item_id + ", timeOut=" + t_timeout + ", timeIn=" + t_timein
            + ", employeeId=" + t_borrower + ", managerOutId=" + t_manokout + ", managerInId=" + t_manokin + "]";
   }
}

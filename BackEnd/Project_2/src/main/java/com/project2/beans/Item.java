
package com.project2.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "Items")
public class Item {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "i_id")
    @SequenceGenerator(name = "i_id", sequenceName = "gen_item_id", allocationSize = 1)
    private int i_id = -1;

    @Column(nullable = false)
    private String i_name;

    private int i_avail = 1;

    @Column(nullable = false)
    private String i_pic = "noURL";

    public Item() {
        super();
    }

    public Item(int id, String name, int available, String picture) {
        super();
        this.i_id = id;
        this.i_name = name;
        this.i_avail = available;
        this.i_pic = picture;
    }

    public int getI_id() {
        return i_id;
    }

    
    public void setI_id(int i_id) {
        this.i_id = i_id;
    }

    
    public String getI_name() {
        return i_name;
    }

    
    public void setI_name(String i_name) {
        this.i_name = i_name;
    }

    
    public int getI_avail() {
        return i_avail;
    }

    
    public void setI_avail(int i_avail) {
        this.i_avail = i_avail;
    }

    
    public String getI_pic() {
        return i_pic;
    }

    
    public void setI_pic(String i_pic) {
        this.i_pic = i_pic;
    }

    @Override
    public String toString() {
        return "Item [I_id=" + i_id + ", I_name=" + i_name + ", I_avail=" + i_avail + ", I_pic=" + i_pic + "]";
    }


}

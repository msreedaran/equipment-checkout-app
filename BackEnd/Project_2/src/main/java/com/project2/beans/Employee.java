
package com.project2.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;


@Entity
@Table(name = "Employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "e_id")
    @SequenceGenerator(name = "e_id", sequenceName = "gen_empl_id", allocationSize = 1)
    private int e_id;

    @Column(nullable = false)
    @NaturalId
    private String e_username;

    @Column(nullable = false)
    private String e_password;

    @Column(nullable = false)
    private String e_fullname;

    private int e_isManager = 0;

    private String e_contact = "";

    
  

    public Employee() {
        super();
    }

    public Employee(int e_id, String e_username, String e_password, String e_fullname, int e_isManager,
            String e_contact) {
        super();
        this.e_id = e_id;
        this.e_username = e_username;
        this.e_password = e_password;
        this.e_fullname = e_fullname;
        this.e_isManager = e_isManager;
        this.e_contact = e_contact;
    }


    public int getE_id() {
        return e_id;
    }


    public void setE_id(int e_id) {
        this.e_id = e_id;
    }


    public String getE_username() {
        return e_username;
    }


    public void setE_username(String e_username) {
        this.e_username = e_username;
    }


    public String getE_password() {
        return e_password;
    }


    public void setE_password(String e_password) {
        this.e_password = e_password;
    }


    public String getE_fullname() {
        return e_fullname;
    }


    public void setE_fullname(String e_fullname) {
        this.e_fullname = e_fullname;
    }


    public int getE_isManager() {
        return e_isManager;
    }


    public void setE_isManager(int e_isManager) {
        this.e_isManager = e_isManager;
    }


    public String getE_contact() {
        return e_contact;
    }


    public void setE_contact(String e_contact) {
        this.e_contact = e_contact;
    }


    @Override
    public String toString() {
        return "Employee [e_id=" + e_id + ", e_username=" + e_username + ", e_password=" + e_password + ", e_fullname="
                + e_fullname + ", e_isManager=" + e_isManager + ", e_contact=" + e_contact +  "]";
    }
}

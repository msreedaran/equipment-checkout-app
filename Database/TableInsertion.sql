-- items


INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('broom');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('pan');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('chair');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('vacuum');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('laptop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');
INSERT INTO items (i_name) VALUES ('desktop');



UPDATE items
    SET i_avail = 0
    WHERE i_id = 3;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 13;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 21;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 1;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 7;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 16;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 19;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 20;
UPDATE items
    SET i_avail = 0
    WHERE i_id = 11;





SELECT * FROM items;



--users

INSERT INTO employees(
    e_id,
    e_username,
    e_password,
    e_fullname,
    e_ismanager)
VALUES (
    0,
    'none',
    'none',
    --utl_raw.cast_to_raw('none'),
    'none',
    0);
    

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_ismanager,
    e_contact)
VALUES (
    'adamr',
    'superpassword',
    --utl_raw.cast_to_raw('superpassword'),
    'Adam Raneiri',
    1,
    'adam@raneri.adam,
    1-800-cal-adam');

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_ismanager,
    e_contact)
VALUES (
    'manjus',
    'password',
    --utl_raw.cast_to_raw('password'),
    'Manju Sreedaran',
    0,
    'manju@sreedaran.manju,
    1-800-da-manju');

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_contact)
VALUES (
    'james',
    'password',
    --utl_raw.cast_to_raw('password'),
    'James Jamesey',
    'dis@email.com,
    1-800-mac-jmes');

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_contact)
VALUES (
    'raison',
    'password',
    --utl_raw.cast_to_raw('password'),
    'Ryan Maison',
    'ryan@maison.fak,
    1-800-not-fake');

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_contact)
VALUES (
    'zoes',
    'password',
    --utl_raw.cast_to_raw('password'),
    'Zooey Nightschanel',
    'zooey@joke.lol,
    1-800-den-ight');

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_contact)
VALUES (
    'jonjon',
    'password',
    --utl_raw.cast_to_raw('password'),
    'Johnson Anjohnson',
    'dry@foot.powder,
    1-800-washo-ft');

INSERT INTO employees(
    e_username,
    e_password,
    e_fullname,
    e_contact)
VALUES (
    'imbored',
    'password',
    --utl_raw.cast_to_raw('password'),
    'Iam Borayd',
    'right@this.sec,
    1-800-imb-ored');

SELECT e_id,e_fullname,e_ismanager,e_contact FROM employees;



--transactions

  -- checked out
INSERT INTO transactions(t_item_id, t_timeout,                          t_borrower, t_manokout)
                 VALUES (16,        TO_DATE('04-16-2019','MM-DD-YYYY'), 2,          1);
    
INSERT INTO transactions(t_item_id, t_timeout,                          t_borrower, t_manokout)
                 VALUES (43,        TO_DATE('04-18-2019','MM-DD-YYYY'), 6,          1);

INSERT INTO transactions(t_item_id, t_timeout,                          t_borrower, t_manokout)
                 VALUES (22,        TO_DATE('04-18-2019','MM-DD-YYYY'), 7,          1);

INSERT INTO transactions(t_item_id, t_timeout,                          t_borrower, t_manokout)
                 VALUES (27,        TO_DATE('05-04-2019','MM-DD-YYYY'), 5,          1);


  -- check out and back in
INSERT INTO transactions(t_item_id, t_timeout, t_timein, t_borrower, t_manokout, t_manokin)
    VALUES (12, TO_DATE('04-20-2019','MM-DD-YYYY'), TO_DATE('04-21-2019','MM-DD-YYYY'), 4, 1, 1);
    
INSERT INTO transactions(t_item_id, t_timeout, t_timein, t_borrower, t_manokout, t_manokin)
    VALUES (37, TO_DATE('04-20-2019','MM-DD-YYYY'), TO_DATE('04-20-2019','MM-DD-YYYY'), 7, 1, 1);

INSERT INTO transactions(t_item_id, t_timeout, t_timein, t_borrower, t_manokout, t_manokin)
    VALUES (49, TO_DATE('04-20-2019','MM-DD-YYYY'), TO_DATE('04-23-2019','MM-DD-YYYY'), 3, 1, 1);

INSERT INTO transactions(t_item_id, t_timeout, t_timein, t_borrower, t_manokout, t_manokin)
    VALUES (81, TO_DATE('04-20-2019','MM-DD-YYYY'), TO_DATE('05-01-2019','MM-DD-YYYY'), 5, 1, 1);

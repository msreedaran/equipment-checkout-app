--DROP FUNCTION GET_PASSHASH;
--DROP TRIGGER HMAC_PASSWORD;
--DROP FUNCTION HMAC_COMPUTE;

DROP TABLE transactions;
DROP TABLE employees;
DROP TABLE items;

DROP SEQUENCE gen_tran_id;
DROP SEQUENCE gen_empl_id;
DROP SEQUENCE gen_item_id;

CREATE SEQUENCE gen_item_id START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE gen_empl_id START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE gen_tran_id START WITH 1 INCREMENT BY 1;

CREATE TABLE items(
    i_id number(3,0) DEFAULT gen_item_id.nextval PRIMARY KEY,
    i_name varchar2(50) NOT NULL,
    i_avail number(1,0) DEFAULT 1 NOT NULL
        CONSTRAINT items_avail_binary CHECK (i_avail IN (0,1)),
    i_pic varchar2(250) DEFAULT 'noURL' NOT NULL
);

CREATE TABLE employees(   
    e_id number(2,0) DEFAULT gen_empl_id.nextval PRIMARY KEY,
    e_username varchar2(12) UNIQUE NOT NULL,
    e_password varchar(32) NOT NULL,
--    e_password raw(32) NOT NULL,
--    e_passiv raw(16) DEFAULT '0' NOT NULL,
    e_fullname varchar2(100) NOT NULL,
    e_ismanager number(1,0) DEFAULT 0 NOT NULL
        CONSTRAINT employees_canapprove_binary CHECK (e_ismanager IN (0,1)),
    e_contact varchar2(100) DEFAULT ' ' NOT NULL
);

CREATE TABLE transactions(
    t_id number(3,0) DEFAULT gen_tran_id.nextval PRIMARY KEY,
    t_item_id number(2,0) NOT NULL REFERENCES items(i_id),
    t_timeout date DEFAULT sysdate NOT NULL,
    t_timein date DEFAULT TO_DATE('01-01-0001', 'MM-DD-YYYY') NOT NULL,
    t_borrower number(2,0) NOT NULL REFERENCES employees(e_id),
    t_manokout number(2,0) NOT NULL REFERENCES employees(e_id),
    t_manokin number(2,0) DEFAULT 0 NOT NULL REFERENCES employees(e_id),
    CONSTRAINT transactions_dupe_checkout UNIQUE(t_item_id, t_timeout),
    CONSTRAINT transactions_invalid_state CHECK(
        ((t_timeout <= t_timein) AND (t_manokin > 0))
     OR ((t_timeout > t_timein) AND (t_manokin = 0))
    )
);



/* 
CREATE OR REPLACE FUNCTION HMAC_COMPUTE(   -- should NOT be directly called via JDBC, used by trigger and function
    passhash IN raw,
    iv_raw   IN raw)
RETURN RAW
IS hashed_raw         RAW (32);
   key_raw            RAW (32);               -- stores 256-bit encryption key
   hash_type       PLS_INTEGER;
   input_raw          RAW (48);
BEGIN
   hash_type  := DBMS_CRYPTO.HMAC_SH256;
   key_raw    := '3d463b7cbb3e064390f90e281bf5e7cfc72898a889a4b90da2dfb4c7b29c0dc7';
   input_raw  := UTL_RAW.CONCAT(iv_raw, passhash);
   hashed_raw := DBMS_CRYPTO.MAC(input_raw, hash_type, key_raw);
      
    RETURN hashed_raw;  
       
    EXCEPTION
        WHEN VALUE_ERROR THEN
            dbms_output.put_line('RAW concat result bigger than 32K');
            
        WHEN OTHERS THEN
            dbms_output.put_line('Unknown error occured');
       
END;
/
CREATE OR REPLACE TRIGGER HMAC_PASSWORD   -- replaces user password with HMAC hash
    BEFORE insert OR update
    ON employees
    FOR EACH row
DECLARE
   key_raw            RAW (32);               -- stores 256-bit encryption key
   iv_raw             RAW (16);
   hashed_raw         RAW (32);               -- stores hashed binary text

BEGIN
   iv_raw     := DBMS_CRYPTO.RANDOMBYTES (16);
   hashed_raw := HMAC_COMPUTE(:NEW.e_password, iv_raw);
   
   :NEW.e_password := hashed_raw;
   :NEW.e_passiv := iv_raw;
   
EXCEPTION
    WHEN VALUE_ERROR THEN
        dbms_output.put_line('RAW concat result bigger than 32K');
        
    WHEN OTHERS THEN
        dbms_output.put_line('Unknown error occured');
   
END;
/
*/
-- CREATE OR REPLACE FUNCTION GET_PASSHASH(  /* CALL THIS in JDBC to verify password given at login
--                                                 user_name, PASSWORD */
/*                                                
    user_name_in IN varchar2,
    passhash_in IN raw
) RETURN raw
IS
    passhash_new raw(32);
    iv_raw       raw(16);
    p1 number;
BEGIN
    SELECT COUNT(*) INTO p1 FROM employees
        WHERE employees.e_username = user_name_in;
    if 0 < p1 THEN
        SELECT e_passiv INTO iv_raw FROM employees WHERE employees.e_username = user_name_IN;
        passhash_new := HMAC_COMPUTE(passhash_in, iv_raw);
        RETURN RAWTOHEX(passhash_new);
    else
        RETURN null;
    end if;    
END;    
/
 */